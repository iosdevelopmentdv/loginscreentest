//
//  UserManager.swift
//  LoginScreen
//
//  Created by Imac on 6/18/19.
//  Copyright © 2019 idev. All rights reserved.
//

import Foundation



class UserManager: NSObject {

    var isUserSignedIn: Bool {
        set { UserDefaultsManager.isUserSignedIn = newValue }
        get { return UserDefaultsManager.isUserSignedIn }
    }
    
    
    var allAvailableWorlds: [GameWorld] = []
    
    // MARK: - Lyfe Cycle
    static let shared = UserManager()
    
    private override init() {
        super.init()
        subscribeToNotifications()
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationsManager.subscribe(subsribeData: [
                (.succesfullAttemptSignInMade,      #selector(succesfullAttemptSignInMade(notification:) )),
                (.unsuccesfullAttemptSignInMade,    #selector(unsuccesfullAttemptSignInMade(notification:))),
                (.userSignedOut,                    #selector(userSignedOut(notification:)))
            ], observer:    self)
    }
    
    // MARK: - Notifications Methods
    @objc
    fileprivate func succesfullAttemptSignInMade(notification: Notification) {
        isUserSignedIn = true
        if let worlds = notification.userInfo?[NotificationUserInfo.availableGameWorlds.rawValue] as? [GameWorld] {
            allAvailableWorlds = worlds
        }
    }
    
    @objc
    fileprivate func  unsuccesfullAttemptSignInMade (notification: Notification) {
        isUserSignedIn = false
        allAvailableWorlds = []
    }
    
    @objc
    fileprivate func  userSignedOut (notification: Notification) {
        isUserSignedIn = false
        allAvailableWorlds = []
    }
}
