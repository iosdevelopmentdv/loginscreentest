//
//  NotificationsManager.swift
//  LoginScreen
//
//  Created by Imac on 6/14/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit

enum NotificationType: String {
    
    // ✅ Common notifications
    // * Sign In - Sign Out
    case loginAttemptStarted
    case succesfullAttemptSignInMade
    case unsuccesfullAttemptSignInMade
    case userSignedOut
    
    // ✅ local ViewModel notifications
    // * Login screen view controller
    case loginScreenVMSignInAttemptStarted
    case loginScreenVMUnsuccesfullAttemptSignInMade
    case loginScreenVMSuccesfullAttemptSignInMade
    case loginScreenVMUserSignedOut
}

enum NotificationUserInfo: String {
    case availableGameWorlds
}



typealias InfoForNotificationSubscribes = (type: NotificationType, selector: Selector)

class NotificationsManager: NSObject {
    
    /// Subscribe object to observe one ane several of observerType notification
    static func subscribe(subsribeType: NotificationType, observer: Any, selector: Selector) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(rawValue: subsribeType.rawValue), object: nil)
    }
    
    /// Subscribe object to observe using array of InfoForNotificationSubscribes. InfoForNotificationSubscribes - tuple where info for subscribing - type and selector, which will be recieved new notifications.
    static func subscribe(subsribeData: [InfoForNotificationSubscribes], observer: Any) {
        subsribeData.forEach{
            NotificationCenter.default.addObserver(observer, selector: $0.selector, name: NSNotification.Name(rawValue: $0.type.rawValue), object: nil)
        }
    }
    
    /// Post notification using ObserverType
    static func postNotification(type: NotificationType, userInfo: [AnyHashable:Any]? = nil) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: type.rawValue), object: nil, userInfo: userInfo)
    }
    
    /// Remove all subscriptions from object
    static func removeAllSubscriptions(object: Any) {
        NotificationCenter.default.removeObserver(object)
    }
}
