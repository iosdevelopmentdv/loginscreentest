//
//  NetworkManager.swift
//  LoginScreen
//
//  Created by Дмитрий Ворко on 6/16/19.
//  Copyright © 2019 idev. All rights reserved.
//

import Foundation
import Alamofire


import Alamofire

typealias JSON = [String:Any]

enum NetworkManagerErrors: Error {
    case failedGetDictionaryFromData
    case inResponseNotData
    case failedGetAllAvailableWorldsFromJson
}



class NetworkManager: NSObject {
    
    private enum NetworkPath: String {
        case signInRequest
        
        static let baseURL = "http://backend1.lordsandknights.com/"
        
        var url: String {
            switch self {
            case .signInRequest:
                return NetworkPath.baseURL + "XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds"
            }
        }
    }
    
    private enum NetworkParameter: String {
        case deviceToken
        case deviceId
        case login
        case password
        
        func defaultParametrValue() -> String {
            switch self {
            case .deviceToken:
                return "\(UIDevice.current.model)-\(UIDevice.current.systemName)-\(UIDevice.current.systemVersion)"
            case .deviceId:
                return UIDevice.current.identifierForVendor?.uuidString ?? ""
            case .password, .login:
                return ""
            }
        }
    }
    
    static func signIn(login: String, password: String, onSuccess: @escaping ([GameWorld])->(), onError: @escaping (Error?)->()) {
        let urlString =  NetworkPath.signInRequest.url
        let parameters: Parameters = [NetworkParameter.deviceToken.rawValue:    NetworkParameter.deviceToken.defaultParametrValue(),
                                      NetworkParameter.deviceId.rawValue:       NetworkParameter.deviceId.defaultParametrValue(),
                                      NetworkParameter.login.rawValue:          login,
                                      NetworkParameter.password.rawValue:       password]
        
        debugPrint("URL = \(urlString)\n PARAMETERS = \(parameters)")
        
        Alamofire.request(urlString, method: .post, parameters: parameters).response { (response) in
            debugPrint("response code - \(response.response?.statusCode ?? NSNotFound)")
            guard response.error == nil else {
                debugPrint("Error sign in \(response.error!.localizedDescription)")
                onError(response.error)
                return
            }
            
            guard let data = response.data else {
                let error = NetworkManagerErrors.inResponseNotData
                debugPrint("Failed sign in. In response not data - \(error)")
                onError(error)
                return
            }
            
            do {
                let json = try PropertyListSerialization.propertyList(from:data, options: [], format: nil) as! [String:Any]
                print("Responded json - \(json)")
                guard let allAvailableWords = json["allAvailableWorlds"] as? [JSON] else  {
                    debugPrint("Error allAvailableWords parse - \(NetworkManagerErrors.failedGetAllAvailableWorldsFromJson)")
                    onError(NetworkManagerErrors.failedGetAllAvailableWorldsFromJson)
                    return
                }
                let gameWorlds = allAvailableWords.map({ (json) -> GameWorld in
                    return GameWorld(json: json)
                })
                onSuccess(gameWorlds)
            } catch {
                onError(error)
                return
            }
        }
    }
    
    
    
    
}
