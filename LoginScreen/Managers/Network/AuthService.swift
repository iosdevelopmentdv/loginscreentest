//
//  AuthService.swift
//  LoginScreen
//
//  Created by Дмитрий Ворко on 6/16/19.
//  Copyright © 2019 idev. All rights reserved.
//

import Foundation


class AuthService {
    
    static func signIn(login: String, password: String) {
        
        NotificationsManager.postNotification(type: .loginAttemptStarted)
        
        NetworkManager.signIn(login: login, password: password, onSuccess: { (gameWorlds) in
            let userInfo = [NotificationUserInfo.availableGameWorlds : gameWorlds]
            NotificationsManager.postNotification(type: .succesfullAttemptSignInMade, userInfo: userInfo)
        }) { (error) in
            if let error = error {
                debugPrint("Failed Sign In - \(error)")
            }
            NotificationsManager.postNotification(type: .unsuccesfullAttemptSignInMade)
        }
    }
    
    static func signOut () {
        NotificationsManager.postNotification(type: .userSignedOut)
    }
    
   
}
