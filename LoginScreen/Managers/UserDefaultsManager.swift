//
//  UserDefaultsManager.swift
//  LoginScreen
//
//  Created by Imac on 6/18/19.
//  Copyright © 2019 idev. All rights reserved.
//

import Foundation

private enum UserDefaultsKeyConstants: String {
    case isUserSignedIn
}


class UserDefaultsManager {
    static var isUserSignedIn: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeyConstants.isUserSignedIn.rawValue)
        }
        
        get {
            let savedValue = UserDefaults.standard.object(forKey: UserDefaultsKeyConstants.isUserSignedIn.rawValue) as? Bool
            return savedValue ?? false
        }
    }
}
