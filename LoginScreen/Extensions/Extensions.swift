//
//  Extensions.swift
//  LoginScreen
//
//  Created by Imac on 6/14/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit



extension UIStoryboard {
    /// Loading view controller from Storyboard
    static func createViewController<T: UIViewController>(type: T.Type) -> T {
        let identifier = String(describing: type)
        let soryboard = UIStoryboard(name: identifier , bundle: nil)
        return soryboard.instantiateViewController(withIdentifier: identifier ) as! T
    }
}


enum RegExp: String {
    case email = "[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}"
}

extension String {
    
    /// Is string matches with one of prepare reg exp.
    func matches(_ regex: RegExp) -> Bool {
        return self.range(of: regex.rawValue, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
