//
//  LoginScreenViewModel.swift
//  LoginScreen
//
//  Created by Imac on 6/14/19.
//  Copyright © 2019 idev. All rights reserved.
//

protocol LoginModelViewProtocol {
    func isValidateTextLogin(text: String) -> Bool
    func isValidateTextPassword(text: String) -> Bool
    func didChangePasswordText(_ text: String)
    func didChangeLoginText(_ text: String)
    func signIn()
}

import UIKit

class LoginScreenViewModel: NSObject, LoginModelViewProtocol {
 
    var loginText = ""
    var passwordText = ""
    

    
    //MARK: - LoginModelViewProtocol methods
    func isValidateTextLogin(text: String) -> Bool {
        return text.matches(.email)
    }
    
    func isValidateTextPassword(text: String) -> Bool {
        return !text.contains(" ") && text != ""
    }
    
    func didChangePasswordText(_ text: String) {
        passwordText = text
    }
    
    func didChangeLoginText(_ text: String) {
        loginText = text
    }
    
    func signIn() {
        AuthService.signIn(login:  loginText, password: passwordText)
    }
}
