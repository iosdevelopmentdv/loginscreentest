//
//  LoginScreenViewController.swift
//  LoginScreen
//
//  Created by Imac on 6/14/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit

fileprivate enum StateOfSelf  {
    case inputDataState
    case startedAttemptToSignInState
}

class LoginScreenViewController: UIViewController {
    
    @IBOutlet fileprivate weak var loginTextField: UITextField!
    @IBOutlet fileprivate weak var passwordTextField: UITextField!
    @IBOutlet fileprivate weak var signInButton: UIButton!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: LoginModelViewProtocol!
   
    fileprivate var selfState: StateOfSelf = .inputDataState {didSet{  }}
    fileprivate var isPasswordFieldValidate = false { didSet{updateValidateOfLoginButton(isValide: isLoginFieldValidate && isPasswordFieldValidate)}}
    fileprivate var isLoginFieldValidate = false { didSet{updateValidateOfLoginButton(isValide: isLoginFieldValidate && isPasswordFieldValidate)}}
    
    
    fileprivate class OwnConstants {
        static let loginButtonColorInvalidate = UIColor.gray
        static let loginButtonColorDefault = UIColor(named: "loginScreenVCLoginButton")!
        static let buttonsCornerRadius: CGFloat = 5.0
        static let buttonsBorderWidth: CGFloat = 0.5
        static let buttonsBorderCOlor = UIColor.black
    }
    
    // MARK: - Lyfe Cycle
    
    static func viewControllerWithViewModel(_ viewModel: LoginModelViewProtocol) ->  LoginScreenViewController {
        let loginScreenVC = UIStoryboard.createViewController(type: LoginScreenViewController.self)
        loginScreenVC.viewModel = viewModel
        return loginScreenVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    deinit {
        NotificationsManager.removeAllSubscriptions(object: self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updatePassword(password: "")
        updateLogin(login: "")
    }
    
    //MARK: - Private
    fileprivate func setupUI() {
        defaultUISettings()
        
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func bindViewModel() {
        NotificationsManager.subscribe(subsribeData: [
            (.loginScreenVMSignInAttemptStarted,            #selector(loginAttemptStarted(notification:))),
            (.loginScreenVMUserSignedOut,                   #selector(userSignedOut(notification:))),
            (.loginScreenVMSuccesfullAttemptSignInMade,     #selector(succesfullAttemptSignInMade(notification:))),
            (.loginScreenVMUnsuccesfullAttemptSignInMade,   #selector(unsuccesfullAttemptSignInMade(notification:))) ],
                                       observer: self )
    }
    
    fileprivate func defaultUISettings () {
        loginTextField.text = ""
        loginTextField.layer.cornerRadius = OwnConstants.buttonsCornerRadius
        loginTextField.layer.borderWidth = OwnConstants.buttonsBorderWidth
        loginTextField.layer.borderColor = OwnConstants.buttonsBorderCOlor.cgColor
        passwordTextField.text = ""
        passwordTextField.layer.cornerRadius = OwnConstants.buttonsCornerRadius
        passwordTextField.layer.borderWidth = OwnConstants.buttonsBorderWidth
        passwordTextField.layer.borderColor = OwnConstants.buttonsBorderCOlor.cgColor
        passwordTextField.isSecureTextEntry = true
        signInButton.layer.cornerRadius = OwnConstants.buttonsCornerRadius
        signInButton.layer.borderWidth = OwnConstants.buttonsBorderWidth
        signInButton.layer.borderColor = OwnConstants.buttonsBorderCOlor.cgColor
        
        navigationController?.isNavigationBarHidden = true
        
        updateValidateOfLoginButton(isValide: isLoginFieldValidate && isPasswordFieldValidate)
    }
    
    fileprivate func updateStateOfSelf(state: StateOfSelf) {
        switch state {
        case .inputDataState:
            activityIndicator.stopAnimating()
        case .startedAttemptToSignInState:
            activityIndicator.startAnimating()
        }
    }
    
    fileprivate func updatePassword(password: String) {
        passwordTextField.text = password
        viewModel.didChangePasswordText(password)
        updateValidateOfLoginButton(isValide: viewModel.isValidateTextPassword(text: password))
    }
    
    fileprivate func updateLogin(login: String) {
        passwordTextField.text = login
        viewModel.didChangePasswordText(login)
        updateValidateOfLoginButton(isValide: viewModel.isValidateTextLogin(text: login))
    }
    
    fileprivate func updateValidateOfLoginButton(isValide: Bool) {
        signInButton.backgroundColor = isValide ? OwnConstants.loginButtonColorDefault : OwnConstants.loginButtonColorInvalidate
    }
    
    // MARK: User Interaction
    @IBAction func passwordTextDidChange(_ sender: UITextField) {
        viewModel.didChangePasswordText(sender.text ?? "")
        isPasswordFieldValidate = viewModel.isValidateTextPassword(text: sender.text ?? "")
        
    }
    @IBAction func loginTextDidChange(_ sender: UITextField) {
        viewModel.didChangeLoginText(sender.text ?? "")
        isLoginFieldValidate = viewModel.isValidateTextLogin(text: sender.text ?? "")
    }
    @IBAction func signInAction(_ sender: UIButton) {
        viewModel.signIn()
    }
    
    // MARK: Notifications
    
    @objc
    fileprivate func loginAttemptStarted (notification: Notification) {
        selfState = .startedAttemptToSignInState;
    }
    
    @objc
    fileprivate func userSignedOut (notification: Notification) {
        selfState = .inputDataState
    }
    
    @objc
    fileprivate func succesfullAttemptSignInMade (notification: Notification) {
        selfState = .inputDataState
        // push next view controller
    }
    
    @objc
    fileprivate func unsuccesfullAttemptSignInMade (notification: Notification) {
        selfState = .inputDataState
    }
    
}

extension LoginScreenViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return selfState == .inputDataState
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        // Prevent entering spaces
        return !string.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}
