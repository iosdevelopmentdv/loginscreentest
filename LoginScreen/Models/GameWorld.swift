//
//  GameWord.swift
//  LoginScreen
//
//  Created by Дмитрий Ворко on 6/16/19.
//  Copyright © 2019 idev. All rights reserved.
//

import Foundation


struct GameWorld {
    let country: String
    let id: Int
    let language: String
    let mapURL: String
    let name: String
    let url: String
    let worldStatus: WorldStatus
    
    init(json: JSON) {
        country = json["country"] as? String ?? ""
        id = json["id"] as? Int ?? NSNotFound
        language = json["language"] as? String ?? ""
        mapURL = json["mapURL"] as? String ?? ""
        name = json["name"] as? String ?? ""
        url = json["url"] as? String ?? ""
        
        if let statusJSON = json["worldStatus"] as? JSON {
            worldStatus = WorldStatus(json: statusJSON)
        } else {
            worldStatus = WorldStatus(description: .unknown, id: NSNotFound)
        }
    }
}

enum GameStatusDescription: String {
    case unknown
    case online
    case offline
}

struct WorldStatus {
    let description: GameStatusDescription
    let id: Int
    
    init(description: GameStatusDescription, id: Int) {
        self.description = description
        self.id = id
    }
    
    init(json: JSON) {
        let descriptionStr = json["description"] as? String ?? ""
        description = GameStatusDescription(rawValue: descriptionStr) ??  GameStatusDescription.unknown
        id = json["id"] as? Int ?? NSNotFound
    }
}
