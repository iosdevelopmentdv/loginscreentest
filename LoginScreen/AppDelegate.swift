//
//  AppDelegate.swift
//  LoginScreen
//
//  Created by Imac on 6/14/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
          IQKeyboardManager.shared.enable = true
        
        // load first view of application
        loadView()
        return true
    }
    
    //MARK: - Private
    fileprivate func loadView() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let loginViewModel = LoginScreenViewModel()
        let loginScreenVC = LoginScreenViewController.viewControllerWithViewModel(loginViewModel)
        let navigationController = UINavigationController.init(rootViewController: loginScreenVC)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        if UserManager.shared.isUserSignedIn {
            
        }
    }
}

